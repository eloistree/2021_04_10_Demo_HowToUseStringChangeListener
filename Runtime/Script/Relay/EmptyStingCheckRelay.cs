using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyStingCheckRelay : MonoBehaviour
{
    public StringEvent m_relay;
    public void PushTextToRelay(string text)
    {

        m_relay.Invoke(text);
    }

}
