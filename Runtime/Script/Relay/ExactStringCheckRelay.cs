using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExactStringCheckRelay : StringTranslatorRelay
{
    public override void TryToParse(string text)
    {
        for (int i = 0; i < m_stringToFoundAndRelay.Count; i++)
        {
            if (m_stringToFoundAndRelay[i].IsEqualTo(ref text))
            {
                m_translationRelay.Invoke(m_stringToFoundAndRelay[i].GetTexToTrigger());
                return;
            }

        }
        m_notFound.Invoke(text);
    }

}
