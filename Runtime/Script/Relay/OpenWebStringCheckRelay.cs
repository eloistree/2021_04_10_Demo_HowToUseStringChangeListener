using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWebStringCheckRelay : MonoBehaviour
{
    public Demo_WebPageOpenerUtility m_webUtility;
    public StringEmitEvent m_notFound;

    public  void TryToParse(string text)
    {
        if (text.IndexOf("=") == 0)
        {
            m_webUtility.Open_MathJS(text.Substring(1));
            return;
        }
        else if (text.IndexOf("w:") == 0)
        {

            m_webUtility.Open_WikiSearch(text.Substring(2));
            return;
        }
        else if (text.IndexOf("y:") == 0)
        {

            m_webUtility.Open_YoutubeSearch(text.Substring(2));
            return;
        }
        else if (text.IndexOf("g:") == 0)
        {

            m_webUtility.Open_GoogleSearch(text.Substring(2));
            return;
        }
        else if (text.IndexOf("o:") == 0)
        {

            m_webUtility.Open_Othographe(text.Substring(2));
            return;
        }
        else if (text.IndexOf("t:") == 0)
        {

            m_webUtility.Open_Translate(text.Substring(2));
            return;
        }
        else if (text.IndexOf("?:") == 0)
        {

            m_webUtility.Open_SearchEveryWhereFor(text.Substring(2));
            return;
        }

        m_notFound.Invoke(text);
    }

}
