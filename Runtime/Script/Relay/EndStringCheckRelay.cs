using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EndStringCheckRelay : StringTranslatorRelay
{
   
    public override void TryToParse(string text)
    {
        for (int i = 0; i < m_stringToFoundAndRelay.Count; i++)
        {
            if (m_stringToFoundAndRelay[i].IsFinishBy(ref text))
            {

                m_translationRelay.Invoke(m_stringToFoundAndRelay[i].GetTexToTrigger());
                
                return;
            }
        }
        m_notFound.Invoke(text);




    }
}


