using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class StringTranslatorRelay : MonoBehaviour
{


    public StringEvent m_translationRelay;
    public StringEvent m_notFound;
    public List<StringToFoundToAlternativeText> m_stringToFoundAndRelay = new List<StringToFoundToAlternativeText>();

    public void Clear()
    {
        m_stringToFoundAndRelay.Clear();
    }
    public void AddToClipboardParser(StringToFoundToAlternativeText value)
    {
        m_stringToFoundAndRelay.Add(value);
    }
    public abstract void TryToParse(string text);


}
[System.Serializable]
public class StringEvent : UnityEvent<string> { }