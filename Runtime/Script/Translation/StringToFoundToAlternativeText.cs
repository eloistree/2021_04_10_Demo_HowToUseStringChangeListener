using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StringToFound
{
    public string m_stringToFound = "";
    public bool m_ignoreCase = true;
}

[System.Serializable]
public class StringToFoundToAlternativeText : StringToFound
{
    public string m_textToTrigger = "";

    public StringToFoundToAlternativeText(string toFound, string toTrigger, bool ignoreCase = true)
    {
        m_stringToFound = toFound;
        m_textToTrigger = toTrigger;
        m_ignoreCase = ignoreCase;
    }

    public string GetTexToTrigger()
    {
        return m_textToTrigger;
    }

    public bool IsEqualTo(ref string text)
    {
        if (m_stringToFound.Length != text.Length)
            return false;
        if (m_ignoreCase)
            return (m_stringToFound.ToLower().IndexOf(text.ToLower()) == 0);
        else
            return (m_stringToFound.IndexOf(text) == 0);

    }
    public bool IsFinishBy(ref string textToLookIn, bool ignoreCase = true)
    {
        return FinishBy(textToLookIn.ToLower(), m_stringToFound, ignoreCase);

    }


        private static bool FinishBy(string textToOverwatch, string textToFound, bool ignoreCase = true)
    {
        if (ignoreCase)
        {
            textToOverwatch = textToOverwatch.ToLower();
            textToFound = textToFound.ToLower();
        }
        if (textToOverwatch.Length < textToFound.Length)
        {
            return false;
        }
        else if (textToOverwatch.Length == textToFound.Length)
        {
            if (textToOverwatch == textToFound)
                return true;
        }
        else if (textToOverwatch.Length > textToFound.Length)
        {
            for (int i = 0; i < textToFound.Length; i++)
            {
                if (textToFound[textToFound.Length - 1 - i] != textToOverwatch[textToOverwatch.Length - 1 - i])
                {
                    return false;
                }

            }
            return true;
        }
        return false;
    }

}