using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InspectorToStringToFoundMono : MonoBehaviour
{


    [TextArea(0,5)]
    [Tooltip("whatToReplace|replaceby")]
    public string m_clipboardTranslation;
    [Tooltip("whatToReplace|replaceby")]
    public TextAsset m_clipboardTranslationText;

    public UnityEvent m_resetEvent;
    public StringToTextTriggerEvent m_found;
    [System.Serializable]
    public class StringToTextTriggerEvent : UnityEvent<StringToFoundToAlternativeText> { }

    public void Start()
    {
        
        m_resetEvent.Invoke();

        AddClipboardExactParsingWithText(m_clipboardTranslation);
        if(m_clipboardTranslationText!=null)
        AddClipboardExactParsingWithText(m_clipboardTranslationText.text);
    }


    public void AddClipboardExactParsingWithText(string textSplitBy, char spliterValue='|', char spliterLineReturn='\n')
    {
        string[] lines = textSplitBy.Split(spliterLineReturn);
        for (int i = 0; i < lines.Length; i++)
        {
            string[] p = lines[i].Split(spliterValue);
           
                if (p.Length == 2) {
                    AddClipboardExactParsing(p[0], p[1]);
                }
            

        }

    }
    public void AddClipboardExactParsing(string whatToFound, string replaceBy)
    {

        m_found.Invoke(new StringToFoundToAlternativeText(whatToFound, replaceBy));
    }
    public void AddClipboardExactParsing(StringToFoundToAlternativeText value)
    {
        m_found.Invoke(value);
    }


}
