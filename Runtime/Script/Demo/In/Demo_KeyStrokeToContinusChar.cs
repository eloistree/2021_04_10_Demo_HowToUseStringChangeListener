using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Demo_KeyStrokeToContinusChar : MonoBehaviour
{
    public UnityEvent<char> m_inputFound;

    void Update()
    {
        for (int i = 0; i < m_keyObserved.Length; i++)
        {
            if (m_keyObserved[i].IsDown())
                m_inputFound.Invoke( m_keyObserved[i].GetChar());
        }
    }
    [System.Serializable]
    public class KeyCodeToChar {
        [SerializeField] char m_char;
        [SerializeField] KeyCode m_keycode;

        public KeyCodeToChar(KeyCode keycode, char @character)
        {
            m_keycode = keycode;
            m_char = character;
        }

        public bool IsDown() { return Input.GetKeyDown(m_keycode); }
        public char GetChar() { return m_char; }
    }
    public KeyCodeToChar[] m_keyObserved = new KeyCodeToChar[] {
    new KeyCodeToChar(KeyCode.Space, ' '),
    new KeyCodeToChar(KeyCode.Alpha0, '0'),
    new KeyCodeToChar(KeyCode.Alpha1, '1'),
    new KeyCodeToChar(KeyCode.Alpha2, '2'),
    new KeyCodeToChar(KeyCode.Alpha3, '3'),
    new KeyCodeToChar(KeyCode.Alpha4, '4'),
    new KeyCodeToChar(KeyCode.Alpha5, '5'),
    new KeyCodeToChar(KeyCode.Alpha6, '6'),
    new KeyCodeToChar(KeyCode.Alpha7, '7'),
    new KeyCodeToChar(KeyCode.Alpha8, '8'),
    new KeyCodeToChar(KeyCode.Alpha9, '9'),
    new KeyCodeToChar(KeyCode.Keypad0, '0'),
    new KeyCodeToChar(KeyCode.Keypad1, '1'),
    new KeyCodeToChar(KeyCode.Keypad2, '2'),
    new KeyCodeToChar(KeyCode.Keypad3, '3'),
    new KeyCodeToChar(KeyCode.Keypad4, '4'),
    new KeyCodeToChar(KeyCode.Keypad5, '5'),
    new KeyCodeToChar(KeyCode.Keypad6, '6'),
    new KeyCodeToChar(KeyCode.Keypad7, '7'),
    new KeyCodeToChar(KeyCode.Keypad8, '8'),
    new KeyCodeToChar(KeyCode.Keypad9, '9'),
    new KeyCodeToChar(KeyCode.A, 'a'),
    new KeyCodeToChar(KeyCode.B, 'b'),
    new KeyCodeToChar(KeyCode.C, 'c'),
    new KeyCodeToChar(KeyCode.D, 'd'),
    new KeyCodeToChar(KeyCode.E, 'e'),
    new KeyCodeToChar(KeyCode.F, 'f'),
    new KeyCodeToChar(KeyCode.G, 'g'),
    new KeyCodeToChar(KeyCode.H, 'h'),
    new KeyCodeToChar(KeyCode.I, 'i'),
    new KeyCodeToChar(KeyCode.J, 'j'),
    new KeyCodeToChar(KeyCode.K, 'k'),
    new KeyCodeToChar(KeyCode.L, 'l'),
    new KeyCodeToChar(KeyCode.M, 'm'),
    new KeyCodeToChar(KeyCode.N, 'n'),
    new KeyCodeToChar(KeyCode.O, 'o'),
    new KeyCodeToChar(KeyCode.P, 'p'),
    new KeyCodeToChar(KeyCode.Q, 'q'),
    new KeyCodeToChar(KeyCode.R, 'r'),
    new KeyCodeToChar(KeyCode.S, 's'),
    new KeyCodeToChar(KeyCode.T, 't'),
    new KeyCodeToChar(KeyCode.U, 'u'),
    new KeyCodeToChar(KeyCode.V, 'v'),
    new KeyCodeToChar(KeyCode.W, 'w'),
    new KeyCodeToChar(KeyCode.X, 'x'),
    new KeyCodeToChar(KeyCode.Y, 'y'),
    new KeyCodeToChar(KeyCode.Z, 'z')
    };
        
        
}
