using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class Demo_InputStringToContinusChar : MonoBehaviour
{
    public UnityString m_inputFound;
    public bool m_onlyAcceptAlpahNum;
    
    void Update()
    {
        if (Input.inputString.Length > 0) {
            if (m_onlyAcceptAlpahNum)
                m_inputFound.Invoke(AlphNumOf(Input.inputString));
            else
                m_inputFound.Invoke(Input.inputString);
        }    
    }

    private string AlphNumOf(string inputString)
    {
        return Regex.Replace(inputString, "[^a-zA-Z0-9\\s]", "");
    }

    [System.Serializable]
    public class UnityString : UnityEvent<string> { }
}
