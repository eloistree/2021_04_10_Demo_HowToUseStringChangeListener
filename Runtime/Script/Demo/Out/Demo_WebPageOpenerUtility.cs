using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_WebPageOpenerUtility : MonoBehaviour
{

   

    public void OpenUrlPage(string url) {
        Application.OpenURL(url);
    }

    public void Open_SearchEveryWhereFor(string subject)
    {
        Open_GoogleSearch(subject);
        Open_YoutubeSearch(subject);
        Open_WikiSearch(subject);
    }
    public void Open_GoogleSearch(string subject)
    {
        OpenUrlPage("https://www.google.com/search?q=" + subject.Replace(" ","+"));
    }
    public void Open_YoutubeSearch(string subject)
    {
        OpenUrlPage("https://www.youtube.com/results?search_query=" + subject.Replace(" ", "+"));

    }
    public void Open_WikiSearch(string subject)
    {

        OpenUrlPage("https://en.wikipedia.org/w/index.php?search=" + subject.Replace(" ", "+"));
    }

    public void Open_MathJS(string equation) {
        OpenUrlPage("http://api.mathjs.org/v4/?expr=" +GetEncodededOf(equation)); 

    }

    private string GetEncodededOf(string equation)
    {
        char [] c = equation.ToCharArray();
        string r="";
        for (int i = 0; i < c.Length; i++)
        {
            r+= GetEncodeEquivalent(c[i]);

        }
        return r;


    }

    public void Open_Othographe(string text)
    {
        OpenUrlPage("https://www.reverso.net/text_translation.aspx");
    }

    private string GetEncodeEquivalent(char c)
    {
        switch (c)
        {
            case ' ': return "%20";
            case '!': return "%21";
            case '\"':       return "%22";
case'#': return "%23";
case'$': return "%24";
case'%': return "%25";
case'&': return "%26";
case'\'': return "%27";
case'(': return "%28";
case')': return "%29";
case'*': return "%2A";
case'+': return "%2B";
case',': return "%2C";
case'-': return "%2D";
case'.': return "%2E";
case'/': return "%2F";
case'0': return "%30";
case'1': return "%31";
case'2': return "%32";
case'3': return "%33";
case'4': return "%34";
case'5': return "%35";
case'6': return "%36";
case'7': return "%37";
case'8': return "%38";
case'9': return "%39";
case':': return "%3A";
case';': return "%3B";
case'<': return "%3C";
case'=': return "%3D";
case'>': return "%3E";
case'?': return "%3F";
case'@': return "%40";
case'A': return "%41";
case'B': return "%42";
case'C': return "%43";
case'D': return "%44";
case'E': return "%45";
case'F': return "%46";
case'G': return "%47";
case'H': return "%48";
case'I': return "%49";
case'J': return "%4A";
case'K': return "%4B";
case'L': return "%4C";
case'M': return "%4D";
case'N': return "%4E";
case'O': return "%4F";
case'P': return "%50";
case'Q': return "%51";
case'R': return "%52";
case'S': return "%53";
case'T': return "%54";
case'U': return "%55";
case'V': return "%56";
case'W': return "%57";
case'X': return "%58";
case'Y': return "%59";
case'Z': return "%5A";
case'[': return "%5B";
case'\\': return "%5C";
case']': return "%5D";
case'^': return "%5E";
case'_': return "%5F";
case'`': return "%60";
case'a': return "%61";
case'b': return "%62";
case'c': return "%63";
case'd': return "%64";
case'e': return "%65";
case'f': return "%66";
case'g': return "%67";
case'h': return "%68";
case'i': return "%69";
case'j': return "%6A";
case'k': return "%6B";
case'l': return "%6C";
case'm': return "%6D";
case'n': return "%6E";
case'o': return "%6F";
case'p': return "%70";
case'q': return "%71";
case'r': return "%72";
case's': return "%73";
case't': return "%74";
case'u': return "%75";
case'v': return "%76";
case'w': return "%77";
case'x': return "%78";
case'y': return "%79";
case'z': return "%7A";
case'{': return "%7B";
case'|': return "%7C";
case'}': return "%7D";
case'~': return "%7E";
case'�': return "%82";
case'�': return "%83";
case'�': return "%84";
case'�': return "%85";
case'�': return "%86";
case'�': return "%87";
case'�': return "%88";
case'�': return "%89";
case'�': return "%8A";
case'�': return "%8B";
case'�': return "%8C";
case'�': return "%8E";
case'�': return "%91";
case'�': return "%92";
case'�': return "%93";
case'�': return "%94";
case'�': return "%95";
case'�': return "%96";
case'�': return "%97";
case'�': return "%98";
case'�': return "%99";
case'�': return "%9A";
case'�': return "%9B";
case'�': return "%9C";
case'�': return "%9E";
case'�': return "%9F";
case'�': return "%A1";
case'�': return "%A2";
case'�': return "%A3";
case'�': return "%A4";
case'�': return "%A5";
case'�': return "%A6";
case'�': return "%A7";
case'�': return "%A8";
case'�': return "%A9";
case'�': return "%AA";
case'�': return "%AB";
case'�': return "%AC";
case'�': return "%AE";
case'�': return "%AF";
case'�': return "%B0";
case'�': return "%B1";
case'�': return "%B2";
case'�': return "%B3";
case'�': return "%B4";
case'�': return "%B5";
case'�': return "%B6";
case'�': return "%B7";
case'�': return "%B8";
case'�': return "%B9";
case'�': return "%BA";
case'�': return "%BB";
case'�': return "%BC";
case'�': return "%BD";
case'�': return "%BE";
case'�': return "%BF";
case'�': return "%C0";
case'�': return "%C1";
case'�': return "%C2";
case'�': return "%C3";
case'�': return "%C4";
case'�': return "%C5";
case'�': return "%C6";
case'�': return "%C7";
case'�': return "%C8";
case'�': return "%C9";
case'�': return "%CA";
case'�': return "%CB";
case'�': return "%CC";
case'�': return "%CD";
case'�': return "%CE";
case'�': return "%CF";
case'�': return "%D0";
case'�': return "%D1";
case'�': return "%D2";
case'�': return "%D3";
case'�': return "%D4";
case'�': return "%D5";
case'�': return "%D6";
case'�': return "%D7";
case'�': return "%D8";
case'�': return "%D9";
case'�': return "%DA";
case'�': return "%DB";
case'�': return "%DC";
case'�': return "%DD";
case'�': return "%DE";
case'�': return "%DF";
case'�': return "%E0";
case'�': return "%E1";
case'�': return "%E2";
case'�': return "%E3";
case'�': return "%E4";
case'�': return "%E5";
case'�': return "%E6";
case'�': return "%E7";
case'�': return "%E8";
case'�': return "%E9";
case'�': return "%EA";
case'�': return "%EB";
case'�': return "%EC";
case'�': return "%ED";
case'�': return "%EE";
case'�': return "%EF";
case'�': return "%F0";
case'�': return "%F1";
case'�': return "%F2";
case'�': return "%F3";
case'�': return "%F4";
case'�': return "%F5";
case'�': return "%F6";
case'�': return "%F7";
case'�': return "%F8";
case'�': return "%F9";
case'�': return "%FA";
case'�': return "%FB";
case'�': return "%FC";
case'�': return "%FD";
case'�': return "%FE";
case'�': return "%FF";
            default:
                return "%20";
        }

    }

    public void Open_Translate(string subject)
    {
        Open_TranslateGoogle(subject);
        Open_TranslateDeepl(subject);
    }
    public void Open_TranslateGoogle(string subject)
    {
        OpenUrlPage("https://translate.google.be/?hl=fr&sl=en&tl=fr&text=" + subject + "&op=translate");
    }
    public void Open_TranslateDeepl(string subject)
    {
        OpenUrlPage("https://www.deepl.com/translator#en/fr/" + GetEncodededOf( subject));
    }
    
}
